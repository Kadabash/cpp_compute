from datetime import datetime
import json
import os
import pathlib
from threading import Thread
from typing import Any, Callable, Dict, List, Sequence


class Response:

    def __init__(self, response_json: str):
        self.response_json = response_json
    
    @property
    def return_value(self):
        response_dict = json.loads(self.response_json)
        return response_dict["return_value"]

    
class CppExecutor:
    """Run computations in a C++ program"""

    _STOP_REQUEST: str = json.dumps(dict())  # Signal for C++ server to stop listening

    def __init__(self, cpp_executable_name: str,
                 request_fifo_file_name: str = "request_fifo",
                 response_fifo_file_name: str = "response_fifo") -> None:
        self._cpp_executable_name = cpp_executable_name
        self._request_fifo_file_name = request_fifo_file_name
        self._response_fifo_file_name = response_fifo_file_name

        self._create_fifo_files()
        self._cpp_executable_thread = Thread(target=self._run_cpp_program,
                                             args=(self._cpp_executable_name,))
        self._cpp_executable_thread.start()

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self._stop_server()
        self._cpp_executable_thread.join()
        self._remove_fifo_files()
        
    def run_computation(self, function_name: str, function_args: Dict[str, Any]) -> Response:
        """Run a computation in C++

        Arguments:
        function  Name of the C++ function to run.
                  Must be implemented and handled correctly in C++ program.
        function_args  Dictionary of argument names and their values.
                       Names and types must match the C++ function signature.
        """
        request_json: str = self._computation_request_json(function_name, function_args)
        self._send_request(request_json)
        return self._get_response()

    def _get_response(self) -> Response:
        # TODO: Consolidate into 'run_computation()'
        with open(self._response_fifo_file_name, "r") as response_fifo:
            return_json = response_fifo.readline()  # type: str

        return Response(return_json)

    def _create_fifo_files(self) -> None:
        for f in [self._request_fifo_file_name, self._response_fifo_file_name]:
            if pathlib.Path(f).exists():
                os.remove(f)
            os.mkfifo(f)

    def _remove_fifo_files(self) -> None:
        for f in [self._request_fifo_file_name, self._response_fifo_file_name]:
            os.remove(f)

    def _send_request(self, request_json: str) -> None:
        with open(self._request_fifo_file_name, "w") as request_fifo:
            print(request_json, file=request_fifo)

    def _stop_server(self) -> None:
        self._send_request(self._STOP_REQUEST)
            
    @staticmethod
    def _computation_request_json(function_name: str, arguments: Dict[str, Any]) -> str:
        """Build a JSON object for a request of computation in C++ program."""
        data = {
            "function_name": function_name,
            "request_time": datetime.now().isoformat()
        }
        data.update(arguments)

        return json.dumps(data)

    @staticmethod
    def _run_cpp_program(executable_name: str) -> None:
        os.system("./" + executable_name)


class Serializable(list):
    """Wrapper around a sequence to allow efficient JSON serialisation

    When passing large sequences in the argument dictionary to 
    CppExecutor.run_computation() they need to be converted to
    a list() first, which is inefficient.
    Passing this wrapper instead saves memory and computation time
    by avoiding copies.
    """
    
    def __init__(self, seq: Sequence) -> None:
        self._seq = seq
        
    def __iter__(self):
        for x in self._seq:
            yield x
            
    def __len__(self):
        return len(self._seq)
