#include <cmath>
#include <numeric>
#include "test_funcs.hpp"


double sinc(double x)
{
    return std::sin(x) / x;
}


double sum_averages(std::vector<double> xs, std::vector<double> ys)
{
    double const x_sum = std::accumulate(xs.begin(), xs.end(), 0.0);
    double const y_sum = std::accumulate(ys.begin(), ys.end(), 0.0);
    
    return x_sum / static_cast<double>(xs.size()) +
           y_sum / static_cast<double>(ys.size());
}


std::vector<double> sinc_all(std::vector<double> xs)
{
    std::vector<double> ys;
    for (auto const x : xs) {
        ys.push_back(std::sin(x) / x);
    }
    return ys;
}
