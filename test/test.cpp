/*
 * Test server for unit tests run from 'test.py'
 */
#include <cmath>
#include <iostream>
#include <numeric>
#include "../include/server.hpp"
#include "test_funcs.hpp"


int main()
{
    Server::Server server;

    server.register_function(sinc, "sinc",
                             std::array<std::string, 1> { "x" });
    server.register_function(sum_averages, "sum_averages",
                             std::array<std::string, 2> { "xs", "ys" });
    server.register_function(sinc_all, "sinc_all",
                             std::array<std::string, 1> {"xs" });
    
    server.run();
    
    return 0;
}
