import cmath
from cpp_executor import CppExecutor, Serializable
import math
import os
from pathlib import Path
import unittest


CPP_EXECUTABLE_NAME = "test/a.out"
EXECUTOR_ARGS = (CPP_EXECUTABLE_NAME,)


class ServerTest(unittest.TestCase):

    def test_run_close(self):
        """Test whether the server runs and terminates without requests"""
        with CppExecutor(*EXECUTOR_ARGS) as ex:
            pass

    def test_float_arg_float_return(self):
        X = 0.0376
        REFERENCE_RESULT = math.sin(X) / X
        
        with CppExecutor(*EXECUTOR_ARGS) as ex:
            result = ex.run_computation("sinc", {"x": X}).return_value

        self.assertTrue(cmath.isclose(result, REFERENCE_RESULT))

    def test_vector_args_float_return(self):
        xs = range(10)
        ys = range(18)
        REFERENCE_RESULT = sum(xs) / len(xs) + sum(ys) / len(ys)

        with CppExecutor(*EXECUTOR_ARGS) as ex:
            result = ex.run_computation("sum_averages",
                                        {"xs": Serializable(xs),
                                         "ys": Serializable(ys)}).return_value

        self.assertTrue(cmath.isclose(result, REFERENCE_RESULT))

    def test_vector_arg_vector_return(self):
        xs = range(1, 10)
        REFERENCE_RESULT = [math.sin(x) / x for x in xs]

        with CppExecutor(*EXECUTOR_ARGS) as ex:
            result = ex.run_computation("sinc_all", {"xs": Serializable(xs)}).return_value

        self.assertEqual(len(REFERENCE_RESULT), len(result))
        all_close = True
        for i in range(len(result)):
            if not cmath.isclose(result[i], REFERENCE_RESULT[i]):
                all_close = False
        self.assertTrue(all_close)
    

if __name__ == "__main__":
    unittest.main()
