#pragma once

#include <vector>


double sinc(double x);

double sum_averages(std::vector<double> xs, std::vector<double> ys);

std::vector<double> sinc_all(std::vector<double> xs);
