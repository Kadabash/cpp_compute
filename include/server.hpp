#pragma once

#include <array>
#include <string>
#include <utility>
#include "../src/server_impl.hpp"


namespace Server
{ 

/** Communicate computation requests from/to FIFO named pipes (Unix only)
 *
 * Requests come as JSON strings. Their format is as follows:
 *   {
 *     "function_name": "..."  # The name of the C++ function to call
 *     "arg1": ...             # Optional: first argument of C++ function with correct name
 *     "arg2": ...
 *     ...                     # More function arguments
 *     "request_time": "..."   # Time the request was made as ISO format string
 *   }
 * Each request must consist of exactly one line and be terminated by a newline.
 *
 * Arguments are named by their C++ names in the JSON object, e.g.
 *    void fun(int a, int b);
 * requires a json object like this:
 *    {
 *      "function_name": "fun"
 *      "a": "5"
 *      "b": "27"
 *      "request_time": "..."
 *    }
 *
 * The return value is written back to the FIFO pipe as a JSON object.
 * Its form is
 *    {
 *      "function_name": "..."  # Name of the function that was called
 *      "request_time": "..."   # Time the request was made as ISO format string
 *      "return_value": ...
 *    }
 *
 * When all computations are done, send an empty JSON object ("{}")
 * to signal that the server can stop listening. 
 */
class Server : ServerImpl
{
public:
    Server()
        : ServerImpl {}
    {}
    
    Server(std::string const& request_fifo_file_name,
           std::string const& response_fifo_file_name)
        : ServerImpl { request_fifo_file_name, response_fifo_file_name }
    {}

    /** Register a C++ function 'f' to be run on request by this server
     * @arg f_name  Name of 'f' as declared in its signature
     * @arg arg_names  Names of the arguments of 'f' as declared in its signature 
     */
    template <typename Func, size_t num_args>
    void register_function(Func f,
                           std::string const& f_name,
                           std::array<std::string, num_args> const& arg_names)
    {
        ServerImpl::register_function<Func, num_args>(std::forward<decltype(f)>(f),
                                                      std::forward<decltype(f_name)>(f_name),
                                                      std::forward<decltype(arg_names)>(arg_names));
    }

    /** Receive computation requests, run them and send their results
     * Requested functions need to be registered first using 'register_function'.
     * Runs and blocks until an empty JSON message is received.
     */
    void run()
    {
        ServerImpl::run();
    }
};

}


