from cpp_compute.cpp_executor import CppExecutor, Serializable


xs = range(0, 100)
ys = [x**2 for x in xs]

with CppExecutor("a.out") as ex:
    result = ex.run_computation("gauss_integral",
                                {"xs": Serializable(xs),  # make 'range' generator serialisable
                                 "ys": ys})  # 'ys' is a list, lists are already serialisable

print("Integral is", result.return_value)
    
