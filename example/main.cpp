#include "cpp_compute/include/server.hpp"
#include <vector>


double gauss_integral(std::vector<double> xs, std::vector<double> ys);


int main()
{
    Server::Server s;

    s.register_function(gauss_integral, "gauss_integral",
                        std::array<std::string, 2> { "xs", "ys" });

    s.run();
}


double gauss_integral(std::vector<double> xs, std::vector<double> ys)
{
    double integral = 0.0;
    
    for (size_t i = 1; i < xs.size(); ++i) {
        double const delta_x = xs[i] - xs[i - 1];
        double const y_avg = (ys[i] + ys[i - 1]) / 2.0;
        integral += delta_x * y_avg;
    }

    return integral;
}
