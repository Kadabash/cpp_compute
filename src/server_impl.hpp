/*
 * Implementation details of include/server.hpp
 */
#pragma once

#include <array>
#include <functional>
#include <fstream>
#include <string>
#include "../external/nlohmann/json.hpp"


namespace Server
{

// JSON object names
namespace JsonKeys
{
    auto constexpr FUNCTION_NAME = "function_name";
    auto constexpr REQUEST_TIME = "request_time";
    auto constexpr RETURN_VALUE = "return_value";
}


/** Runs user function, already includes arguments, returns return value as JSON tree.
 * That way, when a user task is executed, the argument types do not need to be known
 * since they are incapsulated within the function (via lambda capture at TaskFunction creation).
 */
typedef std::function<nlohmann::json(void)> TaskFunction;

/// Wrapper that returns a TaskFunction, into which arguments from the ptree are inserted
typedef std::function<TaskFunction(nlohmann::json)> TaskFunctionWrapper;

}


#include "server_util.hpp"


namespace Server
{

/// Implementation details of class Server, which is derived from this
class ServerImpl
{
protected:
    ServerImpl() = default;
    ServerImpl(std::string const& request_fifo_file_name,
               std::string const& response_fifo_file_name)
        : request_fifo_file_name { request_fifo_file_name },
          response_fifo_file_name { response_fifo_file_name }
    {}

    template <typename Func, size_t num_args>
    void register_function(Func f,
                           std::string const& f_name,
                           std::array<std::string, num_args> const& arg_names);

    void run()
    {
        while (true) {
            std::optional<Task> t = get_request();

            if (not t) {
                // No task -> stop listening:
                break;
            }
    
            try {
                send_response(t.value());
            } catch(...) {
                throw std::runtime_error { "Could not call task function" };
            }
        }
    }

private:
     struct Task
    {
        /* NOTE: Explicit constructor to fulfill 'std::is_constructible'
         * trait required for 'std::make_optional<Task>':
         */
        Task(std::string const& fn, TaskFunction const& tf,
             std::string const& rt)
            : function_name { fn }, task_function { tf },
              request_time_iso_format { rt }
        {
        }

        
        std::string function_name;
        TaskFunction task_function;
        std::string request_time_iso_format;
    };


    std::string const request_fifo_file_name = "request_fifo";
    std::string const response_fifo_file_name= "response_fifo";
    std::map<std::string, TaskFunctionWrapper> user_functions;

    std::optional<Task> from_json_file(std::ifstream& json_file) const
    {
        nlohmann::json json_tree;
        json_file >> json_tree;

        // Empty tree -> no task:
        if (json_tree.size() == 0)
            return std::nullopt;
    
        std::string const function_name = json_tree.at(JsonKeys::FUNCTION_NAME);
        std::string const request_time = json_tree.at(JsonKeys::REQUEST_TIME);

        TaskFunctionWrapper func_wrapper;
        try {
            func_wrapper = user_functions.at(function_name);
        } catch (std::out_of_range const&) {
            throw std::runtime_error { "No function called '" + function_name +
                    "' has been registered as a user function" };
        }

        // Insert arguments from JSON tree into the task function:
        TaskFunction task_func = func_wrapper(json_tree);

        return std::make_optional<Task>(function_name, task_func, request_time);
    }

    /// Read a request from the FIFO file (blocking)
    std::optional<Task> get_request()
    {
        std::ifstream request_fifo { request_fifo_file_name };
        if (not request_fifo.good())
            throw std::runtime_error { "Could not open response FIFO at " +
                    response_fifo_file_name + " for reading" };
    
        return from_json_file(request_fifo);
    }

    /// Execute a task and send its return value to the FIFO file
    void send_response(Task& task)
    {
        nlohmann::json return_tree = task.task_function();  // Obtains computation result
    
        return_tree[JsonKeys::FUNCTION_NAME] = task.function_name;
        return_tree[JsonKeys::REQUEST_TIME] = task.request_time_iso_format;

        std::ofstream response_fifo { response_fifo_file_name };
        if (not response_fifo.good())
            throw std::runtime_error { "Could not open response FIFO at " +
                    response_fifo_file_name + " for writing" };

        response_fifo << return_tree.dump();
    }
};
    

template <typename Func, size_t num_args>
void ServerImpl::register_function(Func f,
                               std::string const& f_name,
                               std::array<std::string, num_args> const& arg_names)
{
    TaskFunctionWrapper runner = ServerUtil::make_computation_runner(std::function { f },
                                                                     arg_names);
    user_functions[f_name] = runner;
}
    
}
