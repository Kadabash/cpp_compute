/*
 * Implementation of template functions in namespace Server
 */
#pragma once

#include <algorithm>
#include <functional>
#include <iterator>
#include "server_impl.hpp"
#include <string>
#include <vector>
#include "../external/nlohmann/json.hpp"


namespace Server
{
namespace ServerUtil
{


/// Obtain argument with primitive JSON type from JSON tree in a type-safe manner
template <typename Arg>
Arg arg_from_tree(nlohmann::json const& request_tree,
                  std::string const& arg_name)
{
    try {
        return request_tree.at(arg_name);
    } catch(...) {
        throw std::runtime_error { "No argument called '" + arg_name +
                                   "' found in request message" };
    }
}
    

/// Make a tuple of function arguments from a JSON request
template <size_t num_names, typename FirstArg, typename ... RestArgs>
std::tuple<FirstArg, RestArgs...>
args_from_request(std::array<std::string, num_names> const& names,
                  nlohmann::json const& request_tree,
                  size_t name_index = 0)
{
    if constexpr (sizeof...(RestArgs) == 0) {
        return std::make_tuple(
            arg_from_tree<FirstArg>(request_tree, names[name_index])
        );
    } else {
        auto const first_arg_tuple = std::make_tuple(
            arg_from_tree<FirstArg>(request_tree, names[name_index])
        );
        return std::tuple_cat(
            first_arg_tuple,
            args_from_request<num_names, RestArgs...>(names, request_tree, name_index + 1)
        );
    }
}


/// Write a return value into a JSON tree
template <typename ReturnType>
nlohmann::json return_value_to_tree(ReturnType const& value)
{
    nlohmann::json return_tree;
    return_tree[JsonKeys::RETURN_VALUE] = value;
    return return_tree;
}


template <typename ReturnType, typename ... ArgTypes>
TaskFunctionWrapper
make_computation_runner(std::function<ReturnType(ArgTypes...)> f,
                        std::array<std::string, sizeof...(ArgTypes)> arg_names)
{
    TaskFunctionWrapper wrapper = [f, arg_names](nlohmann::json const& request_tree) -> TaskFunction
    {
        // Extract arguments from request message:
        std::tuple args = args_from_request<sizeof...(ArgTypes), ArgTypes...>(arg_names, request_tree);

        TaskFunction runner = [f, args = std::move(args)]()
        {
            // Run function:
            ReturnType const return_value = std::apply(f, args);

            // Return value as JSON tree:
            return return_value_to_tree(return_value);
        };

        return runner;
    };

    return wrapper;
}


// Overload of 'make_computation_runner' for functions with 'void' return type
template <typename ... ArgTypes>
TaskFunctionWrapper
make_computation_runner(std::function<void(ArgTypes...)> f,
                        std::array<std::string, sizeof...(ArgTypes)> arg_names)
{
    TaskFunctionWrapper wrapper = [f, arg_names](nlohmann::json const& request_tree) -> TaskFunction
    {
        // Extract arguments from request message:
        std::tuple args = args_from_request<sizeof...(ArgTypes), ArgTypes...>(arg_names, request_tree);

        TaskFunction runner = [f, args = std::move(args)]()
        {
            // Run function:
            std::apply(f, args);

            // Return void as empty JSON tree:
            nlohmann::json return_tree;
            return return_tree;
        };

        return runner;
    };

    return wrapper;
}
    
}
}
