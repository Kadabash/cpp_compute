# Python <-> C++ interface

Outsouce computational tasks from Python to C++.


## Quick start

1. Use a UNIXoid operating system like Linux or BSD. 
   Install dependencies:

    * Python >= 3.6
    * C++ compiler with support for C++17
    
   On Ubuntu just run `sudo apt install python3 g++`.

2. In your project directory, clone the library: `git clone https://gitlab.com/Kadabash/cpp_compute.git`

3. Run `cp cpp_compute/example/* .`

4. Call any C++ function defined and registered in `main.cpp` from `main.py`, following the example
   code in these files.
   
5. Compile the C++ part by running `c++ main.cpp -std=c++17`

6. Run `python3 main.py` to see if everything works.


## Running unit tests

1. Follow **Quick start** through step 2.

2. Run `cd test && c++ test.cpp test_funcs.cpp -std=c++17 && cd ..`

3. In the library root directory, run `python3 -m test.test`


## Notes:

* The arguments and return values of the C++ functions you want to run have to 
  be a numerical primitive (e.g. `double`, `int`), a `std::string`, or a 
  `std::vector` of these.
  Arguments must be taken by value, not by reference.
  
* Requests and responses are passed between the Python program and the C++
  executable in the JSON format via two FIFO named pipe files.
  
* When sending a NumPy array object as the argument to a function taking a
  `std::vector`, wrap the array in a `cpp_executor.Serializable` object
  in the argument dictionary (NumPy arrays are not JSON-serialisable).
